import numpy as np
import random
from numpy import infty
import min_error as m
import nnls as nnls
from kmaxoid import sortMaxoids2d 


def cutler2(X, p, stopCondition, stopIteration):
    [clusters, Z, A, B, min_res] = m.min_error(X, p, stopCondition)
    
    n = X.shape[0]
    
    
    B = np.zeros((n,p))
    for i in range(0,p):
        z = Z[i]
        B[:,i] = nnls.rss(z, X).x
      
    loop = 1
    residual = infty
    while True:
        print 'Iteration: %s' % loop
        if loop > stopIteration:
            print 'Fertig nach %s Iterationen' % loop
            break
        
        currentResidual = np.sum(np.sum((X - np.dot(A.T,Z))**2,axis=0))
        if np.abs(currentResidual - residual) < stopCondition:
            residual = currentResidual
            break
        else:
            residual = currentResidual
            
        for i in range(0,p):
            B[:,i] = nnls.rss3(X, A, B, i)
                        
        Z = np.dot(B.T, X)
        for i in range(0,n):
            x = X[i]
            A[:,i] = nnls.rss(x, Z).x
            
        loop +=1
        
    if m == 2:
        Z = sortMaxoids2d(Z)
        
    return [A, B, Z, residual, loop]
    
def cutler(X, p, stopCondition, stopIteration):
    print 'Start Cutler: '
    
    [n, m] = X.shape
    #take some random X as archetypes
    Z = X[random.sample(range(0,n), p)]
    
    A = np.zeros((p,n))
    for i in range(0,n):
        x = X[i]
        A[:,i] = nnls.rss(x, Z).x
    
    B = np.zeros((n,p))
    for i in range(0,p):
        z = Z[i]
        B[:,i] = nnls.rss(z, X).x
      
    loop = 1
    residual = infty
    while True:
        #print 'Iteration: %s' % loop
        if loop > stopIteration:
            print 'Fertig nach %s Iterationen' % loop
            break
        
        currentResidual = np.sum(np.sum((X - np.dot(A.T,Z))**2,axis=0))**(0.5)
        if np.abs(currentResidual - residual) < stopCondition:
            residual = currentResidual
            break
        else:
            residual = currentResidual
            
        for i in range(0,p):
            B[:,i] = nnls.rss3(X, A, B, i)
                        
        Z = np.dot(B.T, X)
        for i in range(0,n):
            x = X[i]
            A[:,i] = nnls.rss(x, Z).x
            
        loop +=1
        #print 'residual = '
        print residual
        
    if m == 2:
        Z = sortMaxoids2d(Z)
    
    
         
    return [A, B, Z, residual, loop]
    