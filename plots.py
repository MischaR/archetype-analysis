import numpy as np
import matplotlib
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import math
import kmaxoid as km
import cutler as cutler
import re
import scipy.optimize as optimization

matplotlib.rcParams['legend.handlelength'] = 0.5

#colors
blue = 'steelblue'
magenta = '#ff34b3'
green = '#7cfc00'
red = '#FF6347'
#markersize
pointmarkersize = 15
archetypesize=20
lengendsize = 15
maxoids = mlines.Line2D([], [], color=red, marker='^', markersize=lengendsize, label='Maxoids')
archetypes = mlines.Line2D([], [], color=red, marker='^', markersize=lengendsize, label='Archetypes')


def main():
    print 'Die ausgewählten Inhalte werden geplottet'
    
    #plot blob and ring distributed data with mean, mediod and maxoid
    #plot_mean_mediod_maxoid()

    #plot kmaxoid/archetype example
    #plotKmaxoidArchetype()
    
    
    #plot Kmaxoid MovieLens
    #plotMovieLensMaxoids()
    
    #plot archetype Movielens
    #plotMovieLensArchetypes()
    
    #plot 2d Movielens
    #plotArchetypeKmaxoidMovieLens2D()
    
    #Zeitreihenanalyse Konvergenzgeschwindigkeit
    #compareTimeMaxoidArchetype()
 
    
    #Plotte Vergleich der gefunden Maxoids und Archetypes
    #maxoids_archetypes()
     
    
     
   
def plotArchetypeKmaxoidMovieLens2D():
    p = 8
    m = 19
    n = 943
    
    '''
    #Bestimmung welche Archetypes zu welchen Maxoids gehoren
    data_maxoids = open('maxoids8.txt', 'r')
    maxoid = np.zeros((p,m))
    linecount = 0
    for line in data_maxoids:
        vector = re.split('\|', line, m)
        for i in range(0,m):
            maxoid[linecount, i] = float(vector[i])
        linecount += 1
        
    data_archetypes = open('archetypes8.txt', 'r')
    archetype = np.zeros((p,m))
    linecount = 0
    for line in data_archetypes:
        vector = re.split('\|', line, m)
        for i in range(0,m):
            archetype[linecount, i] = float(vector[i])
        linecount += 1
    
    new_order = np.zeros((8,1))
    for i in range(0,8):
        min_distance_index = np.sum((maxoid[i,:] - archetype)**2,1).argmin()
        new_order[i] = min_distance_index
        archetype[min_distance_index,:] = np.infty 
    new_order = new_order.T
    '''
    
    data_A = open('maxoidsA8.txt', 'r')
    kmaxoid_A = np.zeros((n,p))
    linecount = 0
    for line in data_A:
        vector = re.split('\|', line, p)
        for i in range(0,p):
            kmaxoid_A[linecount, i] = float(vector[i])
        linecount += 1
    
    data_A = open('archetypesA8.txt', 'r')
    archetype_A = np.zeros((n,p))
    linecount = 0
    for line in data_A:
        vector = re.split('\|', line, p)
        for i in range(0,p):
            archetype_A[linecount, i] = float(vector[i])
        linecount += 1
    #Umsortieren
    archetype_A = archetype_A[:,[2, 4, 1, 3, 0, 7, 6, 5]] 
    
    pointslegend = mlines.Line2D([], [], color=blue, marker='o', markersize=lengendsize, label='Datenpunkte zur Basis der Archetypes')
    
    W = km.archetype_view2D(p)
    normalized_archetypes = np.dot(W, archetype_A.T)
    V = km.archetype_view2D(p)
    normalized_kmaxoid = np.dot(V, kmaxoid_A.T)
    
    
    plt.figure(1)
    plt.subplot(121)
    plt.title('Archetypal Analysis')
    plt.axis('equal')
    plt.plot(np.append(W[0,:], W[0,0]), np.append(W[1,:], W[1,0]) ,'^-',color=red,markersize=archetypesize,markerfacecolor=red,lw=1)
    plt.plot(normalized_archetypes[0,:], normalized_archetypes[1,:], 'o', color=blue)
    plt.axis([-1.05,1.05,-1.05,1.05])
    plt.legend(handles=[archetypes, pointslegend], loc=2, numpoints = 1)
    
    plt.subplot(122)
    plt.title('k-Maxoids')
    plt.axis('equal')
    plt.plot(np.append(V[0,:], V[0,0]), np.append(V[1,:], V[1,0]) ,'^-',color=red,markersize=archetypesize,markerfacecolor=red,lw=1)
    plt.plot(normalized_kmaxoid[0,:], normalized_kmaxoid[1,:], 'o', color=blue)
    plt.axis([-1.05,1.05,-1.05,1.05])
    pointslegend = mlines.Line2D([], [], color=blue, marker='o', markersize=lengendsize, label='Datenpunkte zur Basis der Maxoids')
    plt.legend(handles=[maxoids, pointslegend], loc=2, numpoints = 1)
    plt.show() 
    
def iterationError():
    timeAnalysis = open('timeAnalysis.txt', 'r')
    
    vector = re.split('\|', timeAnalysis.read())
    n = len(vector)
    times = np.zeros((n,1))
    for i in range(0,n):
        times[i] = float(vector[i])
    times = times.T[0]
    iterations = np.arange(10,10+n)
    
    x0 = np.zeros((1,3))[0]
    sigma = np.ones((1,n))[0]
    
    [a,b,c] = optimization.curve_fit(func, np.arange(10,10+n), times, x0, sigma)[0]
    
    plt.figure()
    plt.plot(iterations, times,'^', color=blue, ms=pointmarkersize)
    plt.plot(iterations, func(iterations, a, b, c),'-', color=red,lw=2)
    plt.xlabel('Anzahl der Datenpunkte')
    plt.ylabel('Rechenzeit von Archetypal Analysis / k-Maxoids')
    plt.show()
    
    
def maxoids_archetypes():
    m = 2
    n = 100
    
    #Anzahl der Archetypes
    p = 4
    
    test_runs = 100;
    
    time_kmaxoid = np.zeros((test_runs,1))
    time_cutler = np.zeros((test_runs,1))
    
    acc_kmaxoid = np.zeros((test_runs,1))
    acc_cutler = np.zeros((test_runs,1))
    
    f, axarr = plt.subplots(1, 2)
    for i in range(0,test_runs):
        points = np.concatenate(np.array([np.random.normal(0.3,0.1,(0.3*n,m)), np.random.normal(0.7,0.1,(0.4*n,m)), np.random.normal(0.5,0.1,(0.3*n,m))]))
        
        [clusters, kmaxoid_Z, A, B, residual, iter] = km.kmaxoid(points, p, 0.001)
        
        [A, B, cutler_Z, residual, iter] = cutler.cutler(points, p, 0.001, 100)
       
        axarr[0].set_title('Archetypes der Archetypal Analysis')
        axarr[0].plot(cutler_Z[:,0], cutler_Z[:,1], '.', color=blue, ms=pointmarkersize)
        axarr[0].axis('equal')
        axarr[0].axis([0, 1, 0, 1])
        axarr[1].set_title('Maxoids des k-Maxoids Algorithmus')
        axarr[1].plot(kmaxoid_Z[:,0], kmaxoid_Z[:,1], '.', color=blue, ms=pointmarkersize)
        axarr[1].axis('equal')
        axarr[1].axis([0, 1, 0, 1])

    plt.show()         

def compareTimeMaxoidArchetype():
    
    timeAnalysis = open('timeAnalysis.txt', 'r')
    
    vector = re.split('\|', timeAnalysis.read())
    n = len(vector)
    times = np.zeros((n,1))
    for i in range(0,n):
        times[i] = float(vector[i])
    times = times.T[0]
    iterations = np.arange(10,10+n)
    
    x0 = np.zeros((1,3))[0]
    sigma = np.ones((1,n))[0]
    
    [a,b,c] = optimization.curve_fit(func, np.arange(10,10+n), times, x0, sigma)[0]
    
    plt.figure()
    plt.plot(iterations, times,'^', color=blue, ms=pointmarkersize)
    plt.plot(iterations, func(iterations, a, b, c),'-', color=red,lw=2)
    plt.xlabel('Anzahl der Datenpunkte')
    plt.ylabel('Rechenzeit von Archetypal Analysis / k-Maxoids')
    plt.show()
    
def func(x, a, b, c):
    return a + b*x + c*x*x   
    
   
def plotMovieLensMaxoids():
    
    p = 8
    m = 19
    n = 943
        
    data_maxoids = open('maxoids8.txt', 'r')
    maxoids = np.zeros((p,m))
    linecount = 0
    for line in data_maxoids:
        vector = re.split('\|', line, m)
        for i in range(0,m):
            maxoids[linecount, i] = float(vector[i])
        linecount += 1
    
    data_A = open('maxoidsA8.txt', 'r')
    A = np.zeros((n,p))
    linecount = 0
    for line in data_A:
        vector = re.split('\|', line, p)
        for i in range(0,p):
            A[linecount, i] = float(vector[i])
        linecount += 1
    
    max_height = maxoids.max()
    genre = ['unknown' , 'Action' , 'Adventure' , 'Animation' , 'Children\'s' , 'Comedy' , 'Crime' , 'Documentary' , 'Drama' , 'Fantasy' ,'Film-Noir' , 'Horror' , 'Musical' , 'Mystery' , 'Romance' , 'Sci-Fi' ,'Thriller' , 'War' , 'Western' ]
    plt.figure()
    for i in range(0,8):
        plt.subplot(811+i)
        plt.bar(range(19),maxoids[i],0.8,color=blue)
        plt.ylim(0,max_height)
        plt.ylabel('Popularitat')
        plt.xticks(np.arange(19)+0.5, genre, fontsize = 7)
    plt.show()
    
def plotMovieLensArchetypes():
    
    p = 8
    m = 19
    n = 943
        
    data_archetypes = open('archetypes8.txt', 'r')
    archetypes = np.zeros((p,m))
    linecount = 0
    for line in data_archetypes:
        vector = re.split('\|', line, m)
        for i in range(0,m):
            archetypes[linecount, i] = float(vector[i])
        linecount += 1
    
    data_A = open('archetypesA8.txt', 'r')
    A = np.zeros((n,p))
    linecount = 0
    for line in data_A:
        vector = re.split('\|', line, p)
        for i in range(0,p):
            A[linecount, i] = float(vector[i])
        linecount += 1
    A = A[:,[2, 4, 1, 3, 0, 7, 6, 5]] 
    
    max_height = archetypes.max()
    genre = ['unknown' , 'Action' , 'Adventure' , 'Animation' , 'Children\'s' , 'Comedy' , 'Crime' , 'Documentary' , 'Drama' , 'Fantasy' ,'Film-Noir' , 'Horror' , 'Musical' , 'Mystery' , 'Romance' , 'Sci-Fi' ,'Thriller' , 'War' , 'Western' ]
    plt.figure()
    for i in range(0,8):
        plt.subplot(811+i)
        plt.bar(range(19),archetypes[i],0.8,color=blue)
        plt.ylim(0,max_height)
        plt.ylabel('Popularitat')
        plt.xticks(np.arange(19)+0.5, genre, fontsize = 7)
    plt.show()
        
def plotKmaxoidArchetype():
    #plot kmaxoid and Archetypal Algorithm
    m = 2
    n = 100
    p = 4
    points = np.concatenate(np.array([np.random.normal(0.3,0.1,(0.3*n,m)), np.random.normal(0.7,0.1,(0.4*n,m)), np.random.normal(0.5,0.1,(0.3*n,m))]))
    [clusters, kmaxoid_Z, A, B, residual, iter] = km.kmaxoid(points, p, 0.001)
    [A, B,cutler_Z, residual, iter] = cutler.cutler2(points, p, 0.001, 100)
    
    plt.figure(3)
    plt.title('Zweidimensionale Darstellung des Datensatzes')
    pointslegend = mlines.Line2D([], [], color=blue, marker='o', markersize=lengendsize, label='Datenpunkte zur Basis der Archetypes')
    V = km.archetype_view2D(p)
    normalized = np.dot(V, A)
    plt.plot(normalized[0,:], normalized[1,:], 'o', color=blue)
    plt.plot(np.append(V[0,:], V[0,0]), np.append(V[1,:], V[1,0]) ,'^-',color=red,markersize=archetypesize,markerfacecolor=red,lw=1)
    plt.axes().set_aspect('equal')
    plt.axis([-1.05,1.05,-1.05,1.05])
    plt.legend(handles=[archetypes, pointslegend], loc=2, numpoints = 1)
    plt.show()
    
    plt.figure(1)
    plt.title('Archetypal Analysis')
    plt.axis('equal')
    plt.plot(points[:,0], points[:,1], '.', color=blue, ms=pointmarkersize)
    plt.plot(np.append(cutler_Z[:,0], cutler_Z[0,0]), np.append(cutler_Z[:,1], cutler_Z[0,1]) ,'^-',color=red,markersize=archetypesize,markerfacecolor=red,lw=1)
    plt.axis([-0.1, 1.1, -0.1, 1.1])
    plt.legend(handles=[archetypes], loc=2, numpoints = 1)
    plt.show()
    
    plt.figure(2)
    plt.subplot(121)
    plt.title('k-Maxoids')
    plt.axis('equal')
    plt.plot(points[:,0], points[:,1], '.', color=blue, ms=pointmarkersize)
    plt.plot(np.append(kmaxoid_Z[:,0], kmaxoid_Z[0,0]), np.append(kmaxoid_Z[:,1], kmaxoid_Z[0,1]) ,'^-',color=red,markersize=archetypesize,markerfacecolor=red,lw=1)
    plt.axis([-0.1, 1.1, -0.1, 1.1])
    plt.legend(handles=[maxoids], loc=2, numpoints = 1)
    
    plt.subplot(122)
    plt.title('Archetypal Analysis')
    plt.axis('equal')
    plt.plot(points[:,0], points[:,1], '.', color=blue, ms=pointmarkersize)
    plt.plot(np.append(cutler_Z[:,0], cutler_Z[0,0]), np.append(cutler_Z[:,1], cutler_Z[0,1]) ,'^-',color=red,markersize=archetypesize,markerfacecolor=red,lw=1)
    plt.axis([-0.1, 1.1, -0.1, 1.1])
    plt.legend(handles=[archetypes], loc=2, numpoints = 1)
    
    plt.show()
    
    
    
def plot_mean_mediod_maxoid():
    #ring-distributed points

    plt.subplot(121)
    plt.title('Gleichverteilte Punktmenge')
    points = np.random.rand(100,2)
    mean = np.mean(points,axis=0)
    mediod = getMediod(points)
    maxoid = getMaxoid(points)
    
    mean_legend = mlines.Line2D([], [], color=magenta, marker='d', markersize=lengendsize, label='Mittelwert')
    mediod_legend = mlines.Line2D([], [], color=green, marker='s', markersize=lengendsize, label='Mediod')
    maxoid_legend = mlines.Line2D([], [], color=red, marker='^', markersize=lengendsize, label='Maxoid')
    plt.legend(handles=[mean_legend, mediod_legend, maxoid_legend], loc=3, numpoints = 1)
    
    plt.plot( points[:,0], points[:,1] ,'.', color = blue, ms = pointmarkersize)
    plt.plot( mean[0], mean[1] ,'d', color = magenta, ms = archetypesize)
    plt.plot( mediod[0], mediod[1] ,'s', color = green, ms = archetypesize)
    plt.plot( maxoid[0], maxoid[1] ,'^', color = red, ms = archetypesize)
    plt.axis('equal')
    
    
    
    plt.subplot(122)
    plt.title('Ringverteilte Punktmenge')
    points = ring(np.random.rand(100,2))
    mean = np.mean(points,axis=0)
    mediod = getMediod(points)
    maxoid = getMaxoid(points)
    
    mean_legend = mlines.Line2D([], [], color=magenta, marker='d', markersize=lengendsize, label='Mittelwert')
    mediod_legend = mlines.Line2D([], [], color=green, marker='s', markersize=lengendsize, label='Mediod')
    maxoid_legend = mlines.Line2D([], [], color=red, marker='^', markersize=lengendsize, label='Maxoid')
    plt.legend(handles=[mean_legend, mediod_legend, maxoid_legend], loc=3, numpoints = 1)
    
    plt.plot( points[:,0], points[:,1] ,'.', color=blue, ms = pointmarkersize)
    plt.plot( mean[0], mean[1] ,'d', color=magenta, ms = archetypesize)
    plt.plot( mediod[0], mediod[1] ,'s', color=green, ms = archetypesize)
    plt.plot( maxoid[0], maxoid[1] ,'^', color=red, ms = archetypesize)
    plt.axis('equal')
    
    plt.show()

def plotRing_mean_mediod_maxoid():
    #ring-distributed points
    plt.figure()
    points = ring(np.random.rand(100,2))
    mean = np.mean(points,axis=0)
    mediod = getMediod(points)
    maxoid = getMaxoid(points)
    
    mean_legend = mlines.Line2D([], [], color=magenta, marker='d', markersize=lengendsize, label='Mittelwert')
    mediod_legend = mlines.Line2D([], [], color=green, marker='s', markersize=lengendsize, label='Mediod')
    maxoid_legend = mlines.Line2D([], [], color=red, marker='^', markersize=lengendsize, label='Maxoid')
    plt.legend(handles=[mean_legend, mediod_legend, maxoid_legend], loc=3, numpoints = 1)
    
    plt.plot( points[:,0], points[:,1] ,'.', color=blue, ms = pointmarkersize)
    plt.plot( mean[0], mean[1] ,'d', color=magenta, ms = archetypesize)
    plt.plot( mediod[0], mediod[1] ,'s', color=green, ms = archetypesize)
    plt.plot( maxoid[0], maxoid[1] ,'^', color=red, ms = archetypesize)
    plt.axis('off')
    plt.show()

def ring(points):
    n = points.shape[0]
    
    for i in range(0,n):
        x = points[i][0]
        y = points[i][1]
        
        points[i][0] = math.cos(x*2*math.pi) + math.sin(0.1*y*2*math.pi)
        points[i][1] = math.sin(x*2*math.pi) + math.cos(0.1*y*2*math.pi)
    return points


def getMediod(points):
    mean = np.mean(points,axis=0)
    mediod =  points[np.sum((points - mean)**2,1).argmin()]
    
    return mediod

def getMaxoid(points):
    mean = np.mean(points,axis=0)
    maxoid =  points[np.sum((points - mean)**2,1).argmax()]
    
    return maxoid

if __name__ == "__main__": 
    main()