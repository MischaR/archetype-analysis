import numpy as np
import random
import math
import copy
import nnls as nnls
from operator import itemgetter

def kmaxoid(points, p, min_shift):
    '''
    Input a given point cloud and and the number p of clusters you want to find
    '''
    
       
    [n, m] = points.shape
    if p > n: raise Exception("ILLEGAL: you need more points than clusters")
    
    #Initialize maxoids with random points
    maxoids = points[random.sample(range(0,n), p)]
    #Indicates to which cluster the point belongs
    #points[i] belongs to clusters[i]
    clusters = np.empty((n,1))
    
    
    #Find the nearest points to the maxoids and set the cluster for each point
    for i in range(0, n):
        point = points[i]
        argmin = np.sum((point - maxoids)**2,1).argmin()
        clusters[i] = argmin
        
    loop = 0
    while True:
        loop += 1
        print 'Iteration: %s' % loop
        clusters = np.empty((n,1))
        for i in range(0, n):
            point = points[i]
            argmin = np.sum((point - maxoids)**2,1).argmin()
            clusters[i] = argmin
        
        max_shift = 0
        for i in range(0,p):
                #Find all Points in this cluster
                clusterpoints = points[np.where(clusters == i)[0]]
                #Find the other maxoids
                other_maxoids = maxoids[np.unique(np.where(maxoids != maxoids[i])[0])]
                
                #Distance
                max_distance = np.sum( np.sum((other_maxoids - clusterpoints[0])**2, axis=1)**(0.5) )
                
                max_point_index = 0
                
                
                clusterpointLength = len(clusterpoints)
                j = 1
                L = 2.
                for j in range(1, clusterpointLength):
                    distance = np.sum( np.sum((other_maxoids - clusterpoints[j])**(L), axis=1)**(1/L) )
                    
                    if distance > max_distance:
                        max_distance = distance
                        max_point_index = j

                shift = np.sum((maxoids[i] - clusterpoints[max_point_index])**(L))**(1/L)
                if shift > max_shift:
                    max_shift = shift
                maxoids[i] = clusterpoints[max_point_index]
                
        if max_shift < min_shift:
            break
        
        if loop > 100:
            break
        
    A = np.zeros((p,n))
    for i in range(0,n):
        x = points[i]
        A[:,i] = nnls.rss(x, maxoids).x
    
    B = np.zeros((n,p))
    for i in range(0,p):
        z = maxoids[i]
        B[:,i] = nnls.rss(z, points).x
        
    residual = np.sum(np.sum((points - np.dot(A.T,maxoids))**2,axis=0))**(0.5)
    
    if m == 2:
        maxoids = sortMaxoids2d(maxoids)        
    return [clusters, maxoids, A, B, residual, loop]

def kmaxoid2(points, k, min_shift, M):
    '''
    Input a given point cloud and and the number k of clusters you want to find
    '''
    iter = 0
    [n, m] = points.shape
    if k > n: raise Exception("ILLEGAL: you need more points than clusters")
    
    #Initialize maxoids with random points
    maxoids = points[random.sample(range(0,n), k)]
    #Indicates to which cluster the point belongs
    #points[i] belongs to clusters[i]
    clusters = np.empty((n,1))
    
    
    #Find the nearest points to the maxoids and set the cluster for each point
    for i in range(0, n):
        point = points[i]
        argmin = np.sum((point - maxoids)**2,1).argmin()
        clusters[i] = argmin
        
        
    while True: 
        iter+=1
        print 'Iteration: %s' %iter
        shift = np.zeros((k,1))
        
        #Update cluster maxoids to the point which is farthest away to the other cluster maxoids
        for i in range(0,k):
            #Find all Points in this cluster
            ''' TESTEN Ob 0 richtig ist!!!!!!!'''
            clusterpoints = points[np.where(clusters == i)[0]]
            #Find the other maxoids
            other_maxoids = maxoids[np.unique(np.where(maxoids != maxoids[i])[0])]
            max_distance = 0
            max_point_index = 0
            max_other_maxoids = np.amax(other_maxoids, axis=0)
            min_other_maxoids = np.amin(other_maxoids, axis=0)
            
            for j in range(0, len(clusterpoints)):
                amax = np.amax(np.concatenate([ [max_other_maxoids], [clusterpoints[j]] ]), axis=0)
                amin = np.amin(np.concatenate([ [min_other_maxoids], [clusterpoints[j]] ]), axis=0)
                vol = np.prod(amax - amin)
                distance = np.sum((other_maxoids - clusterpoints[j])**2)
                if max_distance < distance:
                    max_distance = distance
                    max_point_index = j        
            shift[i] = np.sum((maxoids[i] - clusterpoints[max_point_index])**2)
            maxoids[i] = clusterpoints[max_point_index]
         
         
        #Find the nearest points to the maxoids and set the cluster for each point
        for i in range(0, n):
            nearestCluster = np.sum((points[i] - maxoids)**2,1).argmin()
            clusters[i] = nearestCluster
            
        #Break if the minimum shift of the cluster maxoids is reached
        if max(shift) < min_shift: 
            for i in range(0,k):
                #Find all Points in this cluster
                clusterpoints = points[np.where(clusters == i)[0]]
                #Find the other maxoids
                other_maxoids = maxoids[np.unique(np.where(maxoids != maxoids[i])[0])]
                max_distance = 0
                max_point_index = 0
                
                max_other_maxoids = np.amax(other_maxoids, axis=0)
                min_other_maxoids = np.amin(other_maxoids, axis=0)
                
                for j in range(0, len(clusterpoints)):
                    amax = np.amax(np.concatenate([ [max_other_maxoids], [clusterpoints[j]] ]), axis=0)
                    amin = np.amin(np.concatenate([ [min_other_maxoids], [clusterpoints[j]] ]), axis=0)
                    vol = np.prod(amax - amin)
                    distance = np.sum((other_maxoids - clusterpoints[j])**2) + M*vol
                    if max_distance < distance:
                        max_distance = distance
                        max_point_index = j        
                shift[i] = np.sum((maxoids[i] - clusterpoints[max_point_index])**2)
                maxoids[i] = clusterpoints[max_point_index]
                
        if max(shift) < min_shift: 
            break
        
        if iter > 100:
            break
        '''
        print 'Maxoids'
        print maxoids
        
        #Plot current
        list = ['ro', 'go', 'bo', 'wo', 'ko',"yo"]
        list2 = ['rs', 'gs', 'bs', 'ws', 'ks',"ys"]
        for i in range(0, len(maxoids)):
            ii = np.where(clusters == i)[0]
            clusterpoints = points[ii]
            # Reformat that so all x's are together, all y'z etc.
            plt.plot(clusterpoints[:,0], clusterpoints[:,1], list[i])
            plt.plot(maxoids[i][0], maxoids[i][1], list2[i])
        plt.axis([0, 1, 0, 1])
        plt.show()
        '''
    if m == 2:
        maxoids = sortMaxoids2d(maxoids)
                
    return [clusters, sortMaxoids(maxoids)]

def kmaxoid3(points, p, min_shift):
    '''
    Input a given point cloud and and the number p of clusters you want to find
    '''
    
    [clusters, maxoids, A, B, residual] = kmaxoid(points, p, min_shift)   
    [n, m] = points.shape


    #Find the nearest points to the maxoids and set the cluster for each point
    for i in range(0, n):
        point = points[i]
        argmin = np.sum((point - maxoids)**2,1).argmin()
        clusters[i] = argmin
        
    loop = 0
    while True:
        loop += 1
        print 'Iteration: %s' % loop
        clusters = np.empty((n,1))
        for i in range(0, n):
            point = points[i]
            argmin = np.sum((point - maxoids)**2,1).argmin()
            clusters[i] = argmin
        
        max_shift = 0
        for i in range(0,p):
                #Find all Points in this cluster
                clusterpoints = points[np.where(clusters == i)[0]]
                #Find the other maxoids
                other_maxoids = maxoids[np.unique(np.where(maxoids != maxoids[i])[0])]
                

                clusterpointLength = len(clusterpoints)
                distancesMap = np.zeros((clusterpointLength,2))
                distancesMap[:,0] = range(0,clusterpointLength)
                
                for j in range(1, clusterpointLength):    
                    L = 2.
                    distancesMap[j,1] = np.sum( np.sum((other_maxoids - clusterpoints[j])**(L), axis=1)**(1/L) )
                    
                distancesMap = sorted(distancesMap, key=itemgetter(1), reverse=True)
                
                for ii in range(0, min(5,clusterpointLength)):
                    new_maxoids = copy.copy(maxoids)
                    new_maxoids[i] = clusterpoints[distancesMap[ii][0]]
                    
                    new_A = np.zeros((p,n))
                    for j in range(0,n):
                        x = points[j]
                        new_A[:,j] = nnls.rss(x, new_maxoids).x
                    new_residual = np.sum(np.sum((points - np.dot(A.T,new_maxoids))**2,axis=0))**(0.5)
                    
                    if new_residual < residual:
                        print 'Neuer Maxoid Punkt: '
                        max_shift = residual - new_residual
                        residual = new_residual
                        A = new_A
                        maxoids = new_maxoids
        
                
        if max_shift < min_shift:
            break
        
        if loop > 100:
            break
        
    
    B = np.zeros((n,p))
    for i in range(0,p):
        z = maxoids[i]
        B[:,i] = nnls.rss(z, points).x
        
    residual = np.sum(np.sum((points - np.dot(A.T,maxoids))**2,axis=0))
            
    if m == 2:
        maxoids = sortMaxoids2d(maxoids)
        
    return [clusters, maxoids, A, B, residual]

def archetype_view2D(k):
    '''
    Input integer k: Number of archetypes
    Output 2D vector V: Returns the vertices of a regual polygon with k vertices
    '''
    V = np.zeros((2,k))
    for i in range(0, k):
        V[0,i] = math.cos( ((i+1.0)/k) *2*math.pi)
        V[1,i] = math.sin( ((i+1.0)/k) *2*math.pi)
    '''
    #Nice Plot
    plt.plot(np.append(V[0,:], V[0,0]), np.append(V[1,:], V[1,0]) ,'r-')
    plt.axis([-1, 1, -1, 1])
    plt.show()
    '''   
  
    return V

def kmaxoidIterationError(points, p, min_shift):
    '''
    Input a given point cloud and and the number p of clusters you want to find
    '''
    print 'Start kmaxoid: '
    
       
    [n, m] = points.shape
    if p > n: raise Exception("ILLEGAL: you need more points than clusters")
    
    #Initialize maxoids with random points
    maxoids = points[random.sample(range(0,n), p)]
    #Indicates to which cluster the point belongs
    #points[i] belongs to clusters[i]
    clusters = np.empty((n,1))
    
    
    #Find the nearest points to the maxoids and set the cluster for each point
    for i in range(0, n):
        point = points[i]
        argmin = np.sum((point - maxoids)**2,1).argmin()
        clusters[i] = argmin
        
    loop = 0
    while True:
        loop += 1
        #print 'Iteration: %s' % loop
        clusters = np.empty((n,1))
        for i in range(0, n):
            point = points[i]
            argmin = np.sum((point - maxoids)**2,1).argmin()
            clusters[i] = argmin
        
        max_shift = 0
        for i in range(0,p):
                #Find all Points in this cluster
                clusterpoints = points[np.where(clusters == i)[0]]
                #Find the other maxoids
                other_maxoids = maxoids[np.unique(np.where(maxoids != maxoids[i])[0])]
                
                #Distance
                max_distance = np.sum( np.sum((other_maxoids - clusterpoints[0])**2, axis=1)**(0.5) )
                
                max_point_index = 0
                
                
                clusterpointLength = len(clusterpoints)
                j = 1
                L = 2.
                for j in range(1, clusterpointLength):
                    
                    distance = np.sum( np.sum((other_maxoids - clusterpoints[j])**(L), axis=1)**(1/L) )
                    
                    if distance > max_distance:
                        max_distance = distance
                        max_point_index = j

                shift = np.sum((maxoids[i] - clusterpoints[max_point_index])**(L))**(1/L)
                if shift > max_shift:
                    max_shift = shift
                maxoids[i] = clusterpoints[max_point_index]
                
        if max_shift < min_shift:
            break
        
        if loop > 100:
            break
        
        A = np.zeros((p,n))
        for i in range(0,n):
            x = points[i]
            A[:,i] = nnls.rss(x, maxoids).x
        
        B = np.zeros((n,p))
        for i in range(0,p):
            z = maxoids[i]
            B[:,i] = nnls.rss(z, points).x
            
        residual = np.sum(np.sum((points - np.dot(A.T,maxoids))**2,axis=0))**(0.5)
        #print 'residual = '
        print residual
    
    if m == 2:
        maxoids = sortMaxoids2d(maxoids)        
    return [clusters, maxoids, A, B, residual]

def sortMaxoids(maxoids):
    '''
    Sorts the points the way that the 2 nearest points are next to each other
    '''
    [n, m]= maxoids.shape
    
    temp = np.zeros((m,n))
    for i in range(0,n-1):
        temp = copy.copy(maxoids[i+1])
        argNearestPoint = i+1 + np.sum( (maxoids[i] - maxoids[range(i+1,n)])**2 , 1).argmin()
        maxoids[i+1] = maxoids[argNearestPoint]
        maxoids[argNearestPoint] = temp

    return maxoids 
    
def sortMaxoids2d(maxoids):
    '''
    Sorts the points the way that the 2 nearest points are next to each other
    '''
    leftPointIndex = maxoids[:,0].argmin()
    p = maxoids.shape[0]
    delta = (maxoids[:,1] - maxoids[leftPointIndex,1])/ (maxoids[:,0] - maxoids[leftPointIndex,0] )
    maxoids = maxoids[np.argsort(delta)]
    
    return maxoids 
    
    
    
    