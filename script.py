import numpy as np
import matplotlib.pyplot as plt
import kmaxoid as km
import min_error as min
import cutler as cutler
from time import clock
from min_error import min_error



#points = np.concatenate(np.array([np.random.normal(0.3,0.1,(150,2)), np.random.normal(0.7,0.1,(200,2)), np.random.normal(0.5,0.1,(150,2))]))
#points = np.concatenate(np.array([np.random.normal(0.3,0.1,(30,2)), np.random.normal(0.7,0.1,(40,2)), np.random.normal(0.5,0.1,(30,2))]))
m = 2
n = 100
points = np.concatenate(np.array([np.random.normal(0.3,0.1,(0.3*n,m)), np.random.normal(0.7,0.1,(0.4*n,m)), np.random.normal(0.5,0.1,(0.3*n,m))]))
#points = np.array(np.random.uniform(0,1,(100,2)))
#points = np.array(( (0,1), (2,0), (2,2) ))

#Random Points
print 'points = '
print points

def main():
    
    
    
    n = points.shape[0]
    #Anzahl der Archetypes
    p = 4
    
    t1 = clock() # ----------------------------------- Anfangszeit in s
    [clusters, kmaxoid_Z, A, B, residual] = km.kmaxoidIterationError(points, p, 0.001)
    t2 = clock() # ----------------------------------- Endzeit in s
    dt = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
    print 'Fehler kmaxoid = '
    print residual
    print 'Benoetigte Zeit: '
    print dt
    
    t1 = clock() # ----------------------------------- Anfangszeit in s
    [A, B, cutler_Z, residual] = cutler.cutler(points, p, 0.001, 100)
    t2 = clock() # ----------------------------------- Endzeit in s
    dt = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
    print 'Fehler cutler = '
    print residual
    print 'Benoetigte Zeit: '
    print dt
    
    
    t1 = clock() # ----------------------------------- Anfangszeit in s
    [clusters, min_error_Z, A, B, min_res] = min.min_error(points, p, 0.001)
    t2 = clock() # ----------------------------------- Endzeit in s
    dt = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
    print 'Fehler min_error = '
    print min_res
    print 'Benoetigte Zeit: '
    print dt
    
    t1 = clock() # ----------------------------------- Anfangszeit in s
    [A, B, cutler2_Z, res] = cutler.cutler2(points, p, 0.001, 100)
    t2 = clock() # ----------------------------------- Endzeit in s
    dt = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
    print 'Fehler cutler2 = '
    print res
    print 'Benoetigte Zeit: '
    print dt
    
    
    
    plt.plot(points[:,0], points[:,1], 'bo')
    plt.plot(np.append(kmaxoid_Z[:,0], kmaxoid_Z[0,0]), np.append(kmaxoid_Z[:,1], kmaxoid_Z[0,1]) ,'r-')
    
    plt.figure()
    plt.scatter(points[:,0], points[:,1], c="#364958", s=40, linewidths=.1)
    plt.plot(np.append(kmaxoid_Z[:,0], kmaxoid_Z[0,0]), np.append(kmaxoid_Z[:,1], kmaxoid_Z[0,1]) ,'s-',color="red",markersize=12,markerfacecolor='firebrick')
    plt.xlim(0,1)
    plt.ylim(0,1)
    plt.axes().set_aspect('equal')
    #plt.show()
    
    
    
    f, axarr = plt.subplots(2, 2)
    axarr[0,0].plot(points[:,0], points[:,1], 'bo')
    axarr[0,0].set_title('Standard Cutler')
    axarr[0,0].plot(np.append(cutler_Z[:,0], cutler_Z[0,0]), np.append(cutler_Z[:,1], cutler_Z[0,1]) ,'r-')
    axarr[1,0].plot(points[:,0], points[:,1], 'bo')
    axarr[1,0].set_title('Kmaxoid, Min_Error')
    axarr[1,0].plot(np.append(min_error_Z[:,0], min_error_Z[0,0]), np.append(min_error_Z[:,1], min_error_Z[0,1]) ,'y-')
    axarr[0,1].plot(points[:,0], points[:,1], 'bo')
    axarr[0,1].set_title('Kmaxoid, Min_Error, Cutler')
    axarr[0,1].plot(np.append(cutler2_Z[:,0], cutler2_Z[0,0]), np.append(cutler2_Z[:,1], cutler2_Z[0,1]) ,'g-')
    axarr[1,1].plot(points[:,0], points[:,1], 'bo')
    axarr[1,1].set_title('Kmaxoid')
    axarr[1,1].plot(np.append(kmaxoid_Z[:,0], kmaxoid_Z[0,0]), np.append(kmaxoid_Z[:,1], kmaxoid_Z[0,1]) ,'c-')
    #plt.axis([-0.5, 1.5, -0.5, 1.5]) 
    plt.show()
    
    
    plt.figure()
    V = km.archetype_view2D(p)
    normalized = np.dot(V, A)
    plt.plot(normalized[0,:], normalized[1,:], 'bo')
    plt.plot(np.append(V[0,:], V[0,0]), np.append(V[1,:], V[1,0]) ,'r-')
    plt.axis('off')
    plt.axes().set_aspect('equal')
    plt.show()
    
if __name__ == "__main__": 
    main()