import numpy as np
import scipy.optimize as spo

def rss(x, Z):
    '''
    Description: 
        x: Inputpoint
        Z: Matrix for the Archetypes
        Minimize ||x-Z*a||, sum(a)=1, a_i>=0
        Output: a
    '''
    p = Z.shape[0] 
    
    # rss = ||x - Z*a ||
    # x and z are given
    rss = lambda a: np.sum( (x - np.dot(Z.T, a))**2)
    
    # Sum(a_i)=1
    cons = ({'type': 'eq', 'fun': lambda a: np.sum(a) - 1})
    
    # 0 < a_i < 1 
    bnds = np.zeros((p,2))
    bnds[:,1] = None
    
    # Define Startvector
    startvector = np.ones((p,1))/p
    
    res = spo.minimize(rss, startvector, bounds=bnds, constraints=cons)
    
    #return np.around(res.x, decimals=7)
    return res

def rss3(X, A, B, l):
    '''
    Description: 
        Minimize ||v - Z_l|| 
        Output: b_l
    '''
    n = X.shape[0]
    p = B.shape[1]
    
    v = create_v(X, A, B, l)
    
    # z_l minimize -> find b := B_l
    rss = lambda b: np.sum( (v - np.dot(X.T, b))**2)
    
    # Sum(a_i)=1
    cons = ({'type': 'eq', 'fun': lambda b: np.sum(b) - 1})
    
    # 0 < a_i
    bnds = np.zeros((n,2))
    bnds[:,1] = None
    #bnds = bnds.T
    
    # Define Startvector
    startvector = B[:,l]
    res = spo.minimize(rss, startvector, bounds=bnds, constraints=cons)
    
    #return np.around(res.x, decimals=7)
    return res.x

def create_v(X, A, B, l):
    [n, m] = X.shape
    p = B.shape[1]
    
    Z = np.dot(B[:,range(0,l) + range(l+1,p)].T, X)
    V = np.zeros((n,m))
    sumAi = np.sum(A[:,l]**2)
    if sumAi == 0:
        print 'sumAi = 0'
    for i in range(0,n):
        ZA = np.dot(Z.T,A[ range(0,l) + range(l+1,p) ,i])
        V[i] =  X[i] - ZA 
    v = np.dot( A[l,:], V) / (np.sum(A[l,:]**2))
    return v