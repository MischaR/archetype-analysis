import cutler as cutler
import kmaxoid as km
from time import clock
import numpy as np
import matplotlib
import matplotlib.lines as mlines
import matplotlib.pyplot as plt

matplotlib.rcParams['legend.handlelength'] = 0.5




def main():
    m = 2
    n = 100
    
    #Anzahl der Archetypes
    p = 4
    
    test_runs = 10;
    
    time_kmaxoid = np.zeros((test_runs,1))
    time_cutler = np.zeros((test_runs,1))
    
    acc_kmaxoid = np.zeros((test_runs,1))
    acc_cutler = np.zeros((test_runs,1))
    
    f, axarr = plt.subplots(1, 2)
    for i in range(0,test_runs):
        points = np.concatenate(np.array([np.random.normal(0.3,0.1,(0.3*n,m)), np.random.normal(0.7,0.1,(0.4*n,m)), np.random.normal(0.5,0.1,(0.3*n,m))]))
        
        [clusters, kmaxoid_Z, A, B, residual] = km.kmaxoid(points, p, 0.001)
        
        [A, B, cutler_Z, residual] = cutler.cutler(points, p, 0.001, 100)
       
        axarr[0].set_title('cutler')
        axarr[0].plot(cutler_Z[:,0], cutler_Z[:,1], 'b.')
        axarr[0].axis('equal')
        axarr[0].axis([0, 1, 0, 1])
        axarr[1].set_title('k-maxoids')
        axarr[1].plot(kmaxoid_Z[:,0], kmaxoid_Z[:,1], 'b.')
        axarr[1].axis('equal')
        axarr[1].axis([0, 1, 0, 1])

    
    plt.show()   

if __name__ == "__main__": 
    main()