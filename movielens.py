import re
import numpy as np

user_movie_rating = open('u.data.txt', 'r')
movie_genrevector = open('u.item.txt', 'r')

movie_genrevector_table = {}
for line in movie_genrevector:
    var = re.split('\|', line, 1)
    movie_id = var[0]
    var = re.split('\|', var[1][-38:]) 
    genre_vector = np.zeros((19,1))
    for i in range(0,19):
        genre_vector[i] = float(var[i])
    movie_genrevector_table[movie_id] = genre_vector

user_ratedgenrevector_table = {}
for line in user_movie_rating:
    var = re.split('\s', line, 3)
    user_id = var[0]
    movie_id = var[1]
    rating = int(var[2])
    rated_genre_vector = rating*movie_genrevector_table[movie_id]

    if user_id in user_ratedgenrevector_table:
        user_ratedgenrevector_table[user_id].extend([rated_genre_vector])
    else:
        user_ratedgenrevector_table[user_id] = [rated_genre_vector]
  
user_meanratedgenrevector_table = {}      
for key in user_ratedgenrevector_table:
    meanratedgenrevector = np.zeros((19,1))
    n = 0
    for vector in user_ratedgenrevector_table[key]:
        for i in range(0,19):
            if vector[i] > 0:
                meanratedgenrevector[i] = meanratedgenrevector[i] + vector[i]
                n += 1
    for i in range(0,19):
        if n > 0:
            meanratedgenrevector[i] = meanratedgenrevector[i]/n
            
    user_meanratedgenrevector_table[key] = meanratedgenrevector



user_meanratedgenrevector = open('u.user.meanratedgenre.txt', 'w')
for key in range(1,944):
    user_meanratedgenrevector.write(str(key))
    user_meanratedgenrevector.write('\t')
    meanratedgenrevector = user_meanratedgenrevector_table[str(key)]
    user_meanratedgenrevector.write(str(meanratedgenrevector[0][0]))
    for i in range(1,19):
        user_meanratedgenrevector.write('|')
        user_meanratedgenrevector.write(str(meanratedgenrevector[i][0]))
    user_meanratedgenrevector.write('\n')
    
user_movie_rating.close()
movie_genrevector.close()
user_meanratedgenrevector.close()