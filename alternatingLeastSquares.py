import numpy as np
import scipy.optimize as spo
import copy

R = np.array(((2, 0, 5, 1, 0, 3), (1, 3, 0, 3, 4, 5), (0, 1, 0, 0, 5, 2), (2, 5, 2, 5, 1, 2), (0, 2, 4, 2, 0, 0)))
print R

def alternating_least_squares(R, p, max_iter):
    '''
    input: Matrix R in R^(n x m)
    output: Matrix U in R^(n x k), V in R^(k x m) fuer die R ~ UV gilt
    '''
    
    [n, m] = R.shape
    
    U = np.random.rand(n,p)
    V = np.random.rand(p,m)
    iter = 0
    residual = np.infty
    
    while iter < max_iter:
        iter = iter + 1
        print 'Iteration: %s' % iter
        
        new_residual = 0
        for i in range(0, n):
            indices = np.where(R[i,:] > 0)[0]
            U[i,:], res =  spo.nnls( V[:, indices].T, R[i,indices].T)
            new_residual =+ res
            
        for j in range(0, m):
            indices = np.where(R[:,j] > 0)[0]
            V[:,j], res =  spo.nnls( U[indices, :], R[indices, j])
            new_residual =+ res
        
        if new_residual < 0.001:
            break
        
        if np.abs(residual - new_residual) < 0.0001:
            break
        
        residual = copy.copy(new_residual)
        
        
    return [U, V]
    
[U, V] = alternating_least_squares(R, 4, 100)

print 'R ~'
print np.dot(U,V)
