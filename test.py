import numpy as np
import scipy.optimize as spo
import matplotlib.pyplot as plt
import copy
from operator import itemgetter
from kmaxoid import sortMaxoids2d 
import math

def main():
    
    x = np.random.normal(50, 10, (100,2))
    print x
    x = np.percentile(x, 95, axis = 0)
    print x
    
    
    '''
    maxoids = np.random.rand(100,2)
    print maxoids
    
    maxoids = ring(maxoids)
    
    
    maxoids = sortMaxoids2d(maxoids)
    plt.plot(np.append(maxoids[:,0], maxoids[0,0]), np.append(maxoids[:,1], maxoids[0,1]) ,'.')
    plt.show()
    
    
    data = np.zeros((4,2))
    data[:,0] = range(0,4)
    data[:,1] = [0.123, 0.12030, 0.451, 0.12414]
    
    sorted(data, key=itemgetter(1))
    
    data = sorted(data, key=itemgetter(1), reverse=True)
    print data
    print data[1][0]
    
    print min(10,12)
    '''
    
    '''
    n = 100
    points = np.concatenate(np.array([np.random.normal(0.3,0.1,(30,2)), np.random.normal(0.7,0.1,(40,2)), np.random.normal(0.5,0.1,(30,2))]))
    p = 4
       
    [clusters, maxoids, A] = min_error.min_error(points, 4, 0.001)
    list1 = ['ro', 'go', 'bo', 'wo', 'ko',"yo"]
    list2 = ['rs', 'gs', 'bs', 'ws', 'ks',"ys"]
    for i in range(0, len(maxoids)):
        ii = np.where(clusters == i)[0]
        clusterpoints = points[ii]
        # Reformat that so all x's are together, all y'z etc.
        plt.plot(clusterpoints[:,0], clusterpoints[:,1], list1[i])
        plt.plot(maxoids[i][0], maxoids[i][1], list2[i])
    plt.plot(np.append(maxoids[:,0], maxoids[0,0]), np.append(maxoids[:,1], maxoids[0,1]) ,'r-')
    plt.axis([0, 1, 0, 1])
    plt.show()
    '''

def ring(points):
    n = points.shape[0]
    
    for i in range(0,n):
        x = points[i][0]
        y = points[i][1]
        
        points[i][0] = math.cos(x*2*math.pi) + math.sin(0.1*y*2*math.pi)
        points[i][1] = math.sin(x*2*math.pi) + math.cos(0.1*y*2*math.pi)
    return points

def main2():
    n = 100
    points = np.concatenate(np.array([np.random.normal(0.3,0.1,(30,2)), np.random.normal(0.7,0.1,(40,2)), np.random.normal(0.5,0.1,(30,2))]))
    p = 4
    L = 2.
    maxoids = points[range(0,p)]
    print maxoids
    
    while True:
        clusters = np.empty((n,1))
        for i in range(0, n):
            point = points[i]
            argmin = np.sum((point - maxoids)**2,1).argmin()
            clusters[i] = argmin
        '''   
        list1 = ['ro', 'go', 'bo', 'wo', 'ko',"yo"]
        list2 = ['rs', 'gs', 'bs', 'ws', 'ks',"ys"]
        for i in range(0, len(maxoids)):
            ii = np.where(clusters == i)[0]
            clusterpoints = points[ii]
            # Reformat that so all x's are together, all y'z etc.
            plt.plot(clusterpoints[:,0], clusterpoints[:,1], list1[i])
            plt.plot(maxoids[i][0], maxoids[i][1], list2[i])
        plt.plot(np.append(maxoids[:,0], maxoids[0,0]), np.append(maxoids[:,1], maxoids[0,1]) ,'r-')
        plt.axis([0, 1, 0, 1])
        plt.show()
        '''
            
        max_shift = 0
        for i in range(0,p):
                #Find all Points in this cluster
                clusterpoints = points[np.where(clusters == i)[0]]
                #Find the other maxoids
                other_maxoids = maxoids[np.unique(np.where(maxoids != maxoids[i])[0])]
                
                #Distance
                max_distance = np.sum( np.sum((other_maxoids - clusterpoints[0])**2, axis=1)**(0.5) )
                
                max_point_index = 0
                
                
                clusterpointLength = len(clusterpoints)
                j = 1
                loop2 = 0
                while True:
                #for j in range(1, len(clusterpoints)):
                    if j >= clusterpointLength:
                        break
                    
                    
                    distance = np.sum( np.sum((other_maxoids - clusterpoints[j])**(L), axis=1)**(1/L) )
                    maximize = 1*(distance/max_distance)
                    
                    if maximize > 1.0001:
                        max_distance = distance
                        max_point_index = j
                        j = 0
                    else:
                        j = j+1
                    loop2 += 1
                
                shift = np.sum((maxoids[i] - clusterpoints[max_point_index])**(L))**(1/L)
                if shift > max_shift:
                    max_shift = shift
                maxoids[i] = clusterpoints[max_point_index]
                
        print 'max_shift = '  
        print max_shift
        if max_shift < 0.01:
            break
        
        '''
        list1 = ['ro', 'go', 'bo', 'wo', 'ko',"yo"]
        list2 = ['rs', 'gs', 'bs', 'ws', 'ks',"ys"]
        for i in range(0, len(maxoids)):
            ii = np.where(clusters == i)[0]
            clusterpoints = points[ii]
            # Reformat that so all x's are together, all y'z etc.
            plt.plot(clusterpoints[:,0], clusterpoints[:,1], list1[i])
            plt.plot(maxoids[i][0], maxoids[i][1], list2[i])
        plt.plot(np.append(maxoids[:,0], maxoids[0,0]), np.append(maxoids[:,1], maxoids[0,1]) ,'r-')
        plt.axis([0, 1, 0, 1])
        plt.show()
        '''
      
    print 'Ergebnis!!!!'  
    list1 = ['ro', 'go', 'bo', 'wo', 'ko',"yo"]
    list2 = ['rs', 'gs', 'bs', 'ws', 'ks',"ys"]
    for i in range(0, len(maxoids)):
        ii = np.where(clusters == i)[0]
        clusterpoints = points[ii]
        # Reformat that so all x's are together, all y'z etc.
        plt.plot(clusterpoints[:,0], clusterpoints[:,1], list1[i])
        plt.plot(maxoids[i][0], maxoids[i][1], list2[i])
    plt.plot(np.append(maxoids[:,0], maxoids[0,0]), np.append(maxoids[:,1], maxoids[0,1]) ,'r-')
    plt.axis([0, 1, 0, 1])
    plt.show()
    
    Z = copy.copy(maxoids)
    X = points
    
    A = np.zeros((p,n))
    res = np.zeros((1,n))
    for i in range(0,n):
        x = X[i]
        result = rss1(x, Z)
        A[:,i] = result.x
        res[:,i] = result.fun
        
    print 'res = '
    print res
    
    projectedPointsIndices = np.where(res[0] > 0.0001)[0]
    print projectedPointsIndices
    projectedPoints = points[projectedPointsIndices]
    plt.plot(projectedPoints[:,0], projectedPoints[:,1], 'bo')
    plt.plot(np.append(maxoids[:,0], maxoids[0,0]), np.append(maxoids[:,1], maxoids[0,1]) ,'r-')
    plt.axis([0, 1, 0, 1])
    plt.show()
    
    min_res = np.sum(res)
    for kk in range(0,4):
        maxResPointIndex = res[0].argmax()
        maxResPoint = points[maxResPointIndex]
        
        maxoids[np.sum((maxResPoint - maxoids)**2,1).argmin()] = maxResPoint
        '''
        list1 = ['ro', 'go', 'bo', 'wo', 'ko',"yo"]
        list2 = ['rs', 'gs', 'bs', 'ws', 'ks',"ys"]
        for i in range(0, len(maxoids)):
            ii = np.where(clusters == i)[0]
            clusterpoints = points[ii]
            # Reformat that so all x's are together, all y'z etc.
            plt.plot(clusterpoints[:,0], clusterpoints[:,1], list1[i])
            plt.plot(maxoids[i][0], maxoids[i][1], list2[i])
        plt.plot(np.append(maxoids[:,0], maxoids[0,0]), np.append(maxoids[:,1], maxoids[0,1]) ,'r-')
        plt.axis([0, 1, 0, 1])
        plt.show()
        '''
        
        for i in range(0,n):
            x = X[i]
            result = rss1(x, maxoids)
            A[:,i] = result.x
            res[:,i] = result.fun
          
        print A[0,:]  
        
        dim = points.shape[1]
        is_point_projected = np.zeros((n))
        point_to_projection_plane = {}
        projection_plane_to_point = {}
        
        
        for i in range(0,n):
            if res[0][i] > 0.0000001:
                indices = np.where(A[:,i] > 0.0000001)[0]
                if indices.shape[0] >= dim+1:
                    is_point_projected[i] = False
                else:
                    is_point_projected[i] = True
                    point_to_projection_plane.update({ i : indices })
                    
                    if projection_plane_to_point.get( np.str(indices) ) is None:
                        projection_plane_to_point.update({ np.str(indices) : [i] })
                    else:
                        projection_plane_to_point.get( np.str(indices) ).extend([i]) 
        
        max_projected_plane = ''
        max_projected_count = 0
        for key in projection_plane_to_point:
            indices = projection_plane_to_point.get(key)
            count = len(indices)
            if count > max_projected_count:
                max_projected_count = count
                max_projected_plane = key
        
        print 'max_projected plane'
        print max_projected_plane
        
        print 'new maxoid point'
        new_maxoid_index =  projection_plane_to_point.get(max_projected_plane)[res[0][projection_plane_to_point.get(max_projected_plane)].argmax()]
        print points[new_maxoid_index]
        
        res_sum = np.sum(res)
        if res_sum < min_res:
            min_res = res_sum
            Z = copy.copy(maxoids)
    
    list1 = ['ro', 'go', 'bo', 'wo', 'ko',"yo"]
    list2 = ['rs', 'gs', 'bs', 'ws', 'ks',"ys"]
    for i in range(0, len(maxoids)):
        ii = np.where(clusters == i)[0]
        clusterpoints = points[ii]
        # Reformat that so all x's are together, all y'z etc.
        plt.plot(clusterpoints[:,0], clusterpoints[:,1], list1[i])
        plt.plot(Z[i][0], Z[i][1], list2[i])
    plt.plot(np.append(Z[:,0], Z[0,0]), np.append(Z[:,1], Z[0,1]) ,'r-')
    plt.axis([0, 1, 0, 1])
    plt.show()
    
    np.sum(points, axis=1)
            
def rss1(x, Z):
    '''
    Description: 
        x: Inputpoint
        Z: Matrix for the Archetypes
        Minimize ||x-Z*a||, sum(a)=1, a_i>=0
        Output: a
    '''
    p = Z.shape[0] 
    
    # rss = ||x - Z*a ||
    # x and z are given
    rss = lambda a: np.sum( (x - np.dot(Z.T, a))**2)
    
    # Sum(a_i)=1
    cons = ({'type': 'eq', 'fun': lambda a: np.sum(a) - 1})
    
    # 0 < a_i < 1 
    bnds = np.zeros((p,2))
    bnds[:,1] = None
    
    # Define Startvector
    startvector = np.ones((p,1))/p
    
    res = spo.minimize(rss, startvector, bounds=bnds, constraints=cons)
    
    #return np.around(res.x, decimals=7)
    return res     
    
if __name__ == "__main__": 
    main()