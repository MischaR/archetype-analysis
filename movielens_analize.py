import re
import numpy as np
import kmaxoid as km
import cutler as c
from time import clock


user_meanratedgenrevector = open('u.user.meanratedgenre.txt', 'r')
m = 19
data = np.zeros((943,19))
user = 0
for line in user_meanratedgenrevector:
    vector = re.split('\|', re.split('\t', line, 1)[1], 19)
    for i in range(0,19):
        data[user, i] = vector[i]
    user += 1

'''
n = 10
m = 19
data = np.concatenate(np.array([np.random.normal(0.3,0.1,(0.3*n,m)), np.random.normal(0.7,0.1,(0.4*n,m)), np.random.normal(0.5,0.1,(0.3*n,m))]))
'''
    
    
#Anzahl der Archetypes
p = 8
def main():
    #maxoids()
    cutler()

def maxoids():
    t1 = clock() # ----------------------------------- Anfangszeit in s
    # kmaxoid
    [clusters, kmaxoid_Z, A, B, residual, iter] = km.kmaxoid(data, p, 0.001)
    t2 = clock() # ----------------------------------- Endzeit in s
    dt = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
    print 'Fehler kmaxoid = '
    print residual
    print 'Benoetigte Zeit: '
    print dt
    
    maxoids = open('maxoids8.txt', 'w')
    for maxoid in kmaxoid_Z:
        maxoids.write(str(maxoid[0]))
        for i in range(0,m):
            maxoids.write('|')
            maxoids.write(str(maxoid[i]))
        maxoids.write('\n')
        
    maxoids.close()
    
    kmaxoidsA = open('maxoidsA8.txt', 'w')
    for a in A.T:
        kmaxoidsA.write(str(a[0]))
        for i in range(1,p):
            kmaxoidsA.write('|')
            kmaxoidsA.write(str(a[i]))
        kmaxoidsA.write('\n')
    
    kmaxoidsA.close()

def cutler():
    t1 = clock() # ----------------------------------- Anfangszeit in s
    # cutler
    [A, B, cutler_Z, residual,iter] = c.cutler2(data, p, 0.001, 30)
    t2 = clock() # ----------------------------------- Endzeit in s
    dt = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
    print 'Fehler kmaxoid = '
    print residual
    print 'Benoetigte Zeit: '
    print dt

    archetypes = open('archetypes8.txt', 'w')
    for archetype in cutler_Z:
        archetypes.write(str(archetype[0]))
        for i in range(0,m):
            archetypes.write('|')
            archetypes.write(str(archetype[i]))
        archetypes.write('\n')
        
    archetypes.close()
    
    archetypesA = open('archetypesA8.txt', 'w')
    for a in A.T:
        archetypesA.write(str(a[0]))
        for i in range(1,p):
            archetypesA.write('|')
            archetypesA.write(str(a[i]))
        archetypesA.write('\n')
        
    archetypesA.close()
    
if __name__ == "__main__": 
    main()