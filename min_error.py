import numpy as np
import copy
import kmaxoid as km
import nnls as nnls
import random
from kmaxoid import sortMaxoids2d 

def min_error(points, k, min_shift):
    '''
    Input a given point cloud and and the number k of clusters you want to find
    '''
    [clusters, maxoids, A, B, residual, iter] = km.kmaxoid(points, k, min_shift)
    
    [n, m] = points.shape
    
    A = np.zeros((k,n))
    res = np.zeros((1,n))
    for i in range(0,n):
            x = points[i]
            result = nnls.rss(x, maxoids)
            A[:,i] = result.x
            res[:,i] = result.fun
      
    new_maxoids = copy.copy(maxoids)
    
    dim = points.shape[1]
    
    while True:
        
        is_point_projected = np.zeros((n))
        point_to_projection_plane = {}
        projection_plane_to_point = {}
        
        min_res = np.sum(res[0])
        for i in range(0,n):
            if res[0][i] > 0.0000001:
                indices = np.where(A[:,i] > 0.0000001)[0]
                if indices.shape[0] >= dim+1:
                    is_point_projected[i] = False
                else:
                    is_point_projected[i] = True
                    point_to_projection_plane.update({ i : indices })
                    
                    if projection_plane_to_point.get( np.str(indices) ) is None:
                        projection_plane_to_point.update({ np.str(indices) : [i] })
                    else:
                        projection_plane_to_point.get( np.str(indices) ).extend([i]) 
        
        max_projected_plane = ''
        max_projected_res = 0
        for key in projection_plane_to_point:
            indices = projection_plane_to_point.get(key)
            cur_res = np.sum(res[0][indices])
            if cur_res > max_projected_res:
                max_projected_res = cur_res
                max_projected_plane = key
        
        #mass_point = np.sum(points[projection_plane_to_point.get(max_projected_plane)], axis=0)/len(projection_plane_to_point.get(max_projected_plane))
        maxResPointIndex = projection_plane_to_point.get(max_projected_plane)[res[0][projection_plane_to_point.get(max_projected_plane)].argmax()]
        maxResPoint = points[maxResPointIndex]
        
        is_maxoid_changed = False
        for kk in range(0,10):
            correction_step = 1. / (2**kk)
            #nearest_maxoid = np.sum((mass_point - new_maxoids)**2,1).argmin()
            #new_maxoids[nearest_maxoid] = correction_step*mass_point + (1-correction_step)*new_maxoids[nearest_maxoid]
            nearest_maxoid = np.sum((maxResPoint - new_maxoids)**2,1).argmin()
            new_maxoids[np.sum((maxResPoint - new_maxoids)**2,1).argmin()] = correction_step*maxResPoint + (1-correction_step)*new_maxoids[nearest_maxoid]
            
            new_A = np.zeros((k,n))
            new_res = np.zeros((1,n))
            for i in range(0,n):
                x = points[i]
                result = nnls.rss(x, new_maxoids)
                new_A[:,i] = result.x
                new_res[:,i] = result.fun
                
            new_res_sum = np.sum(new_res[0])
        
            if new_res_sum < min_res:
                min_res = new_res_sum
                res = new_res
                maxoids = copy.copy(new_maxoids)
                A = copy.copy(new_A)
                is_maxoid_changed = True
                break
            
        if is_maxoid_changed == False:
            break
    
    #Find the nearest points to the maxoids and set the cluster for each point
    for i in range(0, n):
        point = points[i]
        argmin = np.sum((point - maxoids)**2,1).argmin()
        clusters[i] = argmin
        
    if m == 2:
        maxoids = sortMaxoids2d(maxoids)          
    return [clusters, maxoids, A, B, min_res]

def min_error2(points, k, min_shift):
    '''
    Input a given point cloud and and the number k of clusters you want to find
    '''
    
    [n, m] = points.shape
    
    maxoids = points[random.sample(range(0,n), k)]
    clusters = np.empty((n,1))
    
    A = np.zeros((k,n))
    res = np.zeros((1,n))
    for i in range(0,n):
            x = points[i]
            result = nnls.rss(x, maxoids)
            A[:,i] = result.x
            res[:,i] = result.fun
      
    new_maxoids = copy.copy(maxoids)
    
    dim = points.shape[1]
    
    while True:
        
        is_point_projected = np.zeros((n))
        point_to_projection_plane = {}
        projection_plane_to_point = {}
        
        min_res = np.sum(res[0])
        for i in range(0,n):
            if res[0][i] > 0.0000001:
                indices = np.where(A[:,i] > 0.0000001)[0]
                if indices.shape[0] >= dim+1:
                    is_point_projected[i] = False
                else:
                    is_point_projected[i] = True
                    point_to_projection_plane.update({ i : indices })
                    
                    if projection_plane_to_point.get( np.str(indices) ) is None:
                        projection_plane_to_point.update({ np.str(indices) : [i] })
                    else:
                        projection_plane_to_point.get( np.str(indices) ).extend([i]) 
        
        max_projected_plane = ''
        max_projected_res = 0
        for key in projection_plane_to_point:
            indices = projection_plane_to_point.get(key)
            cur_res = np.sum(res[0][indices])
            if cur_res > max_projected_res:
                max_projected_res = cur_res
                max_projected_plane = key
        
        #mass_point = np.sum(points[projection_plane_to_point.get(max_projected_plane)], axis=0)/len(projection_plane_to_point.get(max_projected_plane))
        maxResPointIndex = projection_plane_to_point.get(max_projected_plane)[res[0][projection_plane_to_point.get(max_projected_plane)].argmax()]
        maxResPoint = points[maxResPointIndex]
        
        is_maxoid_changed = False
        for kk in range(0,10):
            correction_step = 1. / (2**kk)
            #nearest_maxoid = np.sum((mass_point - new_maxoids)**2,1).argmin()
            #new_maxoids[nearest_maxoid] = correction_step*mass_point + (1-correction_step)*new_maxoids[nearest_maxoid]
            nearest_maxoid = np.sum((maxResPoint - new_maxoids)**2,1).argmin()
            new_maxoids[np.sum((maxResPoint - new_maxoids)**2,1).argmin()] = correction_step*maxResPoint + (1-correction_step)*new_maxoids[nearest_maxoid]
            
            new_A = np.zeros((k,n))
            new_res = np.zeros((1,n))
            for i in range(0,n):
                x = points[i]
                result = nnls.rss(x, new_maxoids)
                new_A[:,i] = result.x
                new_res[:,i] = result.fun
                
            new_res_sum = np.sum(new_res[0])
        
            if new_res_sum < min_res:
                min_res = new_res_sum
                res = new_res
                maxoids = copy.copy(new_maxoids)
                A = copy.copy(new_A)
                is_maxoid_changed = True
                break
            
        if is_maxoid_changed == False:
            break
    
    #Find the nearest points to the maxoids and set the cluster for each point
    for i in range(0, n):
        point = points[i]
        argmin = np.sum((point - maxoids)**2,1).argmin()
        clusters[i] = argmin
              
    B = np.zeros((n,k))
    for i in range(0,k):
        z = maxoids[i]
        B[:,i] = nnls.rss(z, points).x
        
    if m == 2:
        maxoids = sortMaxoids2d(maxoids)    
    return [clusters, maxoids, A, B, min_res]