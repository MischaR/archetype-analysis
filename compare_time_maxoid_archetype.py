import numpy as np
import kmaxoid as km
import cutler as cutler
from time import clock

def compareTimeMaxoidArchetype():
    testcase_max = 35
    repetitions = 15
    
    m = 2
    p = 4
    points_number = np.zeros((testcase_max,1))
    relativetime = np.zeros((testcase_max,1))
    for n in range(10,10+testcase_max):
        
        times = np.zeros((repetitions,1))
        for i in range(0,repetitions):
            points = np.concatenate(np.array([np.random.normal(0.3,0.1,(0.3*n,m)), np.random.normal(0.7,0.1,(0.4*n,m)), np.random.normal(0.5,0.1,(0.3*n,m))]))
            t1 = clock() # ----------------------------------- Anfangszeit in s
            [clusters, kmaxoid_Z, A, B, residual] = km.kmaxoid(points, p, 0.001)
            t2 = clock() # ----------------------------------- Endzeit in s
            kmaxoid_dt = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
    
            
            t1 = clock() # ----------------------------------- Anfangszeit in s
            [A, B, cutler_Z, residual] = cutler.cutler(points, p, 0.001, 100)
            t2 = clock() # ----------------------------------- Endzeit in s
            cutler_dt = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
            
            times[i] = cutler_dt/kmaxoid_dt
        
        points_number[n-10] = n
        relativetime[n-10] = np.sum(times)/repetitions
     
       
    timeAnalysis = open('timeAnalysis.txt', 'w')
    timeAnalysis.write(str(relativetime[0][0]))
    for i in range(1,len(relativetime)):
        timeAnalysis.write('|')
        timeAnalysis.write(str(relativetime[i][0]))
    timeAnalysis.write('\n')

    timeAnalysis.close()
    
compareTimeMaxoidArchetype()