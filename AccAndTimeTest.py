import cutler as cutler
import kmaxoid as km
from time import clock
import numpy as np
import matplotlib
import matplotlib.lines as mlines
import matplotlib.pyplot as plt

matplotlib.rcParams['legend.handlelength'] = 0.5




def main():
    m = 2
    n = 100
    
    #Anzahl der Archetypes
    p = 4
    
    test_runs = 100;
    
    time_kmaxoid = np.zeros((test_runs,1))
    time_cutler = np.zeros((test_runs,1))
    
    acc_kmaxoid = np.zeros((test_runs,1))
    acc_cutler = np.zeros((test_runs,1))
    
    for i in range(0,test_runs):
        points = np.concatenate(np.array([np.random.normal(0.3,0.1,(0.3*n,m)), np.random.normal(0.7,0.1,(0.4*n,m)), np.random.normal(0.5,0.1,(0.3*n,m))]))
        
        t1 = clock() # ----------------------------------- Anfangszeit in s
        [clusters, kmaxoid_Z, A, B, residual] = km.kmaxoid(points, p, 0.001)
        t2 = clock() # ----------------------------------- Endzeit in s
        time_kmaxoid[i] = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
        acc_kmaxoid[i] = residual
        
        
        t1 = clock() # ----------------------------------- Anfangszeit in s
        [A, B, cutler_Z, residual] = cutler.cutler(points, p, 0.001, 100)
        t2 = clock() # ----------------------------------- Endzeit in s
        time_cutler[i] = t2 - t1 # ---------- Zeitdifferenz = Endzeit - Anfangszeit
        acc_cutler[i] = residual
    
    kmaxoid_legend = mlines.Line2D([], [], color='blue', marker='d', markersize=15, label='k-maxoids')
    cutler_legend = mlines.Line2D([], [], color='green', marker='s', markersize=15, label='cutler')
    plt.legend(handles=[kmaxoid_legend, cutler_legend], loc=1, numpoints = 1)
    
    plt.plot( time_kmaxoid, acc_kmaxoid ,'bd')
    plt.plot( time_cutler, acc_cutler ,'gs')
    plt.xlabel('Used Time')
    plt.ylabel('Error')
    plt.show()   

if __name__ == "__main__": 
    main()